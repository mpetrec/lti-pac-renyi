clearvars -except big_n
close all
clc
lse=@(x) max(x)+log(sum(exp(x-max(x))));
% add matlab scripts located in 'ExplicitConstructionScripts' folder, so
% that matlab can find them.
oldpath=path;
% newpath=path(oldpath,'ExplicitConstructionScripts\');
path(oldpath,'functions/')
% after code finishes it will restore Matlab search paths to what was set
% before, if you stop the code before it can reset the path you can run
% path(oldpath)
N_threads=6; % setting this to above one will use parallel processing
if N_threads>1
cellfcn=@(a,b) cellParFun(a,b,N_threads); % custom function which splits the input vector
% into N_threads equal parts
else
cellfcn=@cellfun;% Built-in Matlab '                cellfun' function
end
% load robot_arm.dat
% u=detrend(robot_arm(:,1))
% y=detrend(robot_arm(:,2))
% data=iddata([y,u])
% data2=iddata(u,y)
% %data2=detrend(glass)
% %data=iddata([data2.OutputData,data2.InputData])
% 
% sys_true=n4sid(data,4)
% sys_true=pem(data,sys_true)
% figIndex=[1,1]; % initialisation for automatic positioning of the figures on the screen
% makefigs=false;
% plotSecondFigure=true;
 saveFileName='pac_sqrtn_real.mat'


maxN=1e5; % how many synthetic data points to generate
% note big number was chosen to illustrate the convergence, however it will
% have negligable difference (if maxN is atleast big enough), whereas increasing processing time
% significantly
N=maxN;
%N=1e4
pac_n=1;
Nf_delta=1000;
MH_thin=2;
Ns=logspace(1,5,20); % computing bounds over different N, specify the points of N
Ns=floor(Ns);
Ns=unique(Ns);
% lambda for gibbs distribution to compute the Renyi based bound
% can be a constant
% Lambdas=(1).*ones(1,numel(Ns)); %5.8e-04
% Or increasing with N
Lambdas=log(Ns+1); %sqrt(Ns);% log(Ns+1);%
delta=0.1;
% n=2;%size(f.A,1);
p=1;
%
fg.A=[0.16   -0.3
      0   -0.05];
fg.B=[ 0.33   -0.75
      0   -0.09]; 
fg.C=[1,1;
       0,1];
 fg.D=eye(2);
 fg.Q=eye(2);
 %fg.Q=[ 0.9    0.3%
%0.3    4.15];


fg.n=2;
fg.p=1;
fg.q=1;
fg.l1_sqr=normL1(fg)^2;

%
% fg.n=2;
fg.p=1;
fg.q=1;
fg.n=size(fg.A,1);
%fg.A=sys_true.A;
%fg.B=sys_true.K;
%fg.C=sys_true.C;
%fg.D=eye(fg.p+fg.q);
%fg.Q=sys_true.NoiseVariance;
%fg.l1_sqr=normL1(fg)^2;
% fg.mQ=max(eig(fg.Q));
Qeg=fg.Q;
Atr=fg.A;
Ktr=fg.B;
Ctr=fg.C;
%optimal predictor from Automatica paper
D0=Qeg(1:p,p+1:end)/Qeg(p+1:end,p+1:end);
A_hat=[fg.A(1,1), fg.A(1,2)-fg.B(1,2)*fg.C(2,2)-fg.B(1,1)*D0*fg.C(1,2); ...
       zeros(1,1), fg.A(2,2)-fg.B(2,2)*fg.C(2,2)];
B_hat=[fg.B(1,2)+fg.B(1,1)*D0;...
       fg.B(2,2)];
C_hat=[fg.C(1,1), fg.C(1,2)-D0*fg.C(2,2)];
%opt_pred=n4sid(data2,3)
%opt_pred=pem(data2,opt_pred)
f_opt.A=A_hat;f_opt.B=B_hat;f_opt.C=C_hat;f_opt.D=D0;
%f_opt.A=opt_pred.A;f_opt.B=opt_pred.B;f_opt.C=opt_pred.C;f_opt.D=opt_pred.D;
%%big_n=size(A_hat,1)
%from eq. 30 in Automatica paper, and using
%es=y-E[y|H_]=y-y_hat, we get
f0_=f_opt;
f0_.A=A_hat-[fg.B(1,1);0]*C_hat;
f0_.B=[[fg.B(1,1);0],B_hat];
f0_.C=C_hat;
f0_.D=[0,D0];

%% compute Bounds for each N
%close all
%fw_big=f_opt;
fyw_big=f_opt;
%fyw_big.A=f_opt.A-opt_pred.K*opt_pred.C
%fyw_big.B=[opt_pred.K,f_opt.B-opt_pred.K*opt_pred.D];
%fyw_big.D=[zeros(size(f_opt.C,1),size(f_opt.C,1)),f_opt.D]

% fyw_big.A=diag([0.2,0.3,0.4]);
% fyw_big.B=[1;1;1];
% fyw_big.C=[1,1.3,1.4];


A=fyw_big.A;
P=dlyap(A',eye(size(A,1))*0.1,[],eye(size(A))*max(abs(eig(A)))^0.5);
T=sqrtm(P);

fyw_big.A=T*fyw_big.A*inv(T);
fyw_big.B=T*fyw_big.B;
fyw_big.C=fyw_big.C*inv(T);


%f_opt=fyw_big


% f0s={f_opt,f0_};
f0s={fyw_big,fyw_big};
fgs={fg,fg};

%return
% ws={w,[y;w]};
propStep=[0.1^2,0.1^2];

% acceptance rate should be around 23.4%
% Code requires "Control system toolbox" 
%big_n=2

for q=1:2
f0s{q} = customMH_burnin(f0s{q},@(f) log_priorCenteredSynth(f,fyw_big),@(f) proprnd(f,propStep(q)),100,MH_thin);        
end

%% plot set-up
 figs{1}=stdFigure(69,pac_n,...
    'Name','E_rho|L-L_hat| and scaled-down Bounds',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.absL_L_hat,@(obj,BoundScale) obj.Bounds.*BoundScale},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}|\\mathcal{L}(f)-\\hat{\\mathcal{L}}_N(f)|,\\; \\sigma_\\delta=%4.2e$',i,obj.sigma_nabla),@(obj,i,BoundScale) sprintf('$\\omega_%i,\\; r_N^R/%4.2f$',i,1/BoundScale)},...
    'Location','eastoutside',...
    'Title',@(BoundScale) strjoin({'$\rho(f)\propto exp(-\lambda_N\hat{\mathcal{L}}_N(f))$, $\lambda_N=\sqrt{N}$',sprintf('   BoundScale=%4.2f',1/BoundScale)}));%initialised with just 1 plot
figs{end+1}=stdFigure(figs{end}.figId+1,pac_n,...
    'Name','step Delta',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.absL_L_hat_delta},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i$, absL\\_L\\_hat\\_delta ',i)},...
    'Location','eastoutside');%initialised with just 1 plot
figs{end+1}=stdFigure(figs{end}.figId+1,pac_n,...
    'Name','EL and EL_hat',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.E_rho_L,@(obj) obj.E_rho_L_hat},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}\\mathcal{L}(f) $',i),...
                  @(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}\\hat{\\mathcal{L}}_N(f) $',i)},...
    'Location','eastoutside');%initialised with just 1 plot


%% initialising pac Objects
%         [f0s{q}.Gf,f0s{q}.M,f0s{q}.g,f0s{q}.barGf1,f0s{q}.barGf2]=Gf_new(f0s{q});

q=1;
pac=cell(pac_n,1);
pac_n=1
for i=1:pac_n
pac{i}=PAC(Ns,[],[],delta,fgs{q},f0s{q},@(f) log_priorCenteredSynth (f,fyw_big),@proprnd,propStep(q),MH_thin,f_opt);
pac{i}.generateData(N,'Gaussian');%maxN
%nTest=size(data,1)/2;
%nTrain=size(data,1)-nTest;
%y_train=data(:,1).y';
%w_train=data(:,2).y';
%w_train=[y_train;w_train];
%y_test=data(nTrain+1:size(data,1),1).y';
%w_test=data(nTrain+1:size(data,1),2).y';
%w_test=[y_test;w_test];
%pac{i}.set_TrainTest(y_test,w_test,y_train,w_train);
pac{i}.setPosterior('Gibbs',Lambdas);% 'Gibbs',lambda

end

%return
%%

for iter=1:1
        %done=pac_n;
        disp('Iteration')
        iter
        fs=cell(0,0);
        Constants=cell(0,0);
        newSamples=false;
        for i=1:pac_n
            if true %pac{i}.sigma_nabla<1e-5
                %done=done-1;
                for k=1:numel(figs)
                    figs{k}.setStatus(i,'done')
                end
                 %continue;
            end
            
            for k=1:numel(figs)
                figs{k}.setStatus(i,'active')
            end
            drawnow
            pause(0.01);

            disp("New Samples")
            disp(newSamples)
            if newSamples==false
                pac{i}.improveMCestimate(Nf_delta);
                fs=pac{i}.fs_prior;
                Constants=pac{i}.Constants;
            %    newSamples=true;
            else
                pac{i}.improveMCestimate(Nf_delta,fs,Constants);
            end

            BoundScale=0;
            for j=1:pac_n
                alpha=max(pac{j}.absL_L_hat./pac{j}.Bounds);
                BoundScale=max(alpha,BoundScale);
            end
BoundScale=1;
            for k=1:numel(figs)
                figs{k}.updatePlots(pac,BoundScale);
            end
            for k=1:numel(figs)
                figs{k}.setStatus(setdiff(1:pac_n,i),'none')
                figs{k}.setStatus(i,'just done')
            end

            drawnow
            save(saveFileName,'pac')
        end
        %if done==0
        %    break;
        %end
end


