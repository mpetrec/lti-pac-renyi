% acceptance rate should be around 23.4%
% Code requires "Control system toolbox" 
%big_n=2

for q=1:2
f0s{q} = customMH_burnin(f0s{q},@(f) log_priorCentered(f,fyw_big),@(f) proprnd(f,propStep(q)),100,MH_thin);        
end

%% plot set-up
 figs{1}=stdFigure(69,pac_n,...
    'Name','E_rho|L-L_hat| and scaled-down Bounds',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.absL_L_hat,@(obj,BoundScale) obj.Bounds.*BoundScale},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}|\\mathcal{L}(f)-\\hat{\\mathcal{L}}_N(f)|,\\; \\sigma_\\delta=%4.2e$',i,obj.sigma_nabla),@(obj,i,BoundScale) sprintf('$\\omega_%i,\\; r_N^R/%4.2f$',i,1/BoundScale)},...
    'Location','eastoutside',...
    'Title',@(BoundScale) strjoin({'$\rho(f)\propto exp(-\lambda_N\hat{\mathcal{L}}_N(f))$, $\lambda_N=\sqrt{N}$',sprintf('   BoundScale=%4.2f',1/BoundScale)}));%initialised with just 1 plot
figs{end+1}=stdFigure(figs{end}.figId+1,pac_n,...
    'Name','step Delta',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.absL_L_hat_delta},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i$, absL\\_L\\_hat\\_delta ',i)},...
    'Location','eastoutside');%initialised with just 1 plot
figs{end+1}=stdFigure(figs{end}.figId+1,pac_n,...
    'Name','EL and EL_hat',...
    'XData',Ns,...
    'plotFcn',{@(obj) obj.E_rho_L,@(obj) obj.E_rho_L_hat},...
    'plotLegend',{@(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}\\mathcal{L}(f) $',i),...
                  @(obj,i) sprintf('$\\omega_%i,\\; E_{f\\sim\\rho}\\hat{\\mathcal{L}}_N(f) $',i)},...
    'Location','eastoutside');%initialised with just 1 plot


%% initialising pac Objects
%         [f0s{q}.Gf,f0s{q}.M,f0s{q}.g,f0s{q}.barGf1,f0s{q}.barGf2]=Gf_new(f0s{q});

q=1;
pac=cell(pac_n,1);
for i=1:pac_n
pac{i}=PAC(Ns,[],[],delta,fgs{q},f0s{q},@(f) log_priorCentered (f,fyw_big),@proprnd,propStep(q),MH_thin,f_opt);
pac{i}.generateData(N,'Gaussian');%maxN
nTest=size(data,1)/2;
nTrain=size(data,1)-nTest;
y_train=data(:,1).y';
w_train=data(:,2).y';
w_train=[y_train;w_train];
y_test=data(nTrain+1:size(data,1),1).y';
w_test=data(nTrain+1:size(data,1),2).y';
w_test=[y_test;w_test];
%pac{i}.set_TrainTest(y_test,w_test,y_train,w_train);
pac{i}.setPosterior('Gibbs',Lambdas);% 'Gibbs',lambda

end

%return
%%

for iter=1:2
        %done=pac_n;
        disp('Iteration')
        iter
        fs=cell(0,0);
        Constants=cell(0,0);
        newSamples=false;
        for i=1:pac_n
            if true %pac{i}.sigma_nabla<1e-5
                %done=done-1;
                for k=1:numel(figs)
                    figs{k}.setStatus(i,'done')
                end
                 %continue;
            end
            
            for k=1:numel(figs)
                figs{k}.setStatus(i,'active')
            end
            drawnow
            pause(0.01);

            disp("New Samples")
            disp(newSamples)
            if newSamples==false
                pac{i}.improveMCestimate(Nf_delta);
                fs=pac{i}.fs_prior;
                Constants=pac{i}.Constants;
                newSamples=true;
            else
                pac{i}.improveMCestimate(Nf_delta,fs,Constants);
            end

            BoundScale=0;
            for j=1:pac_n
                alpha=max(pac{j}.absL_L_hat./pac{j}.Bounds);
                BoundScale=max(alpha,BoundScale);
            end
BoundScale=1;
            for k=1:numel(figs)
                figs{k}.updatePlots(pac,BoundScale);
            end
            for k=1:numel(figs)
                figs{k}.setStatus(setdiff(1:pac_n,i),'none')
                figs{k}.setStatus(i,'just done')
            end

            drawnow
            save(saveFileName,'pac')
        end
        %if done==0
        %    break;
        %end
end


