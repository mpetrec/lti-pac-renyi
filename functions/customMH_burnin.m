function f = customMH_burnin(f0,log_pdf_fcn,proprnd_fcn,Nf_burnin,MH_thin)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    %% Burn-in (default 10% of n)
    % Nf_burnin=ceil(n/10);
    lu_th=log(rand(1,Nf_burnin*MH_thin+MH_thin));
    f=f0;
    [old_lpos,~,f]=log_pdf_fcn(f);
    
    for i=1:Nf_burnin
        for thin_i=1:MH_thin
            fp=proprnd_fcn(f);
            [lp,~,fp]=log_pdf_fcn(fp);
        
            lH=lp-old_lpos;
            if lu_th(MH_thin*i+thin_i)<lH 
                %keep
                f=fp;
                old_lpos=lp;
            else
                %don't keep
            end
        end
    end
end