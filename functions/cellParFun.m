function res = cellParFun(fcn,cvec,MAX_THREADS)
%cellParFun Imitates cellfun function
%   Decides on 'optimal' number of threads
%   to use, based on how long it takes to process 1.
%   
%cellParFun(@(f) empLossNs(f,y,ws{q},Ns),fs_prior{q},N_threads)

% MAX_THREADS=10;
THREAD_OVERHEAD=0.2; 

% res=nan(size(cvec));
tic
res=fcn(cvec{1});
t=toc;
if size(res,2)>1
    res=res';
end
res=[res,nan(size(res,1),size(cvec,2)-1)];
estTime=numel(cvec)*t;
cvec=cvec(2:end);


% estTotalTime=N_THREADS*THREAD_OVERHEAD+t*numel(cvec)/N_THREADS
% destdn=THREAD_OVERHEAD-t*numel(cvec)
N_THREADS=min(round(sqrt(estTime/THREAD_OVERHEAD),0),MAX_THREADS);
N_THREADS=max(N_THREADS,1);

vecs=cell(1,N_THREADS);
outs=cell(1,N_THREADS);
items_per_Thread=ceil(numel(cvec)/N_THREADS);
for i=1:N_THREADS
    vecs{i}=cvec(1:min(items_per_Thread,end));
    cvec=cvec((items_per_Thread+1):end);
end
% t0=tic;
parfor id=1:N_THREADS
%     t_id=tic;
    outs{id}=cell2mat(cellfun(fcn,vecs{id},'uniformOutput',false));
%     t=toc(t_id);
%     fprintf('Thread %i took %f s for %i items\n',id,t,numel(vecs{id}))
end
% t_total=toc(t0);
% fprintf('In total it took: %f s\n',t_total)

c_idx=2;
for i=1:N_THREADS
    res(:,c_idx:(c_idx+size(outs{i},2)-1))=outs{i};
    c_idx=c_idx+size(outs{i},2);
end

end