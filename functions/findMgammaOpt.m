function [M,gamma] = findMgammaOpt(A)
%UNTITLED7 find M,gamma s.t. |A^k|<= M*gamma^k
%   Detailed explanation goes here


%gamma=max(abs(eig(A)));

%P=dlyap(A',eye(size(A,1)),[],eye(size(A,1))*max(abs(eig(A)))^0.5);
%M=max(abs(eig(P)))/min(abs(eig(P)));

%return
empirical_k=20;



seq=nan(1,empirical_k+1);
seq(1)=1;%norm(A,2);
tempA=eye(size(A));%A;
for j=2:empirical_k+1
tempA=tempA*A;
seq(j)=norm(tempA,2);
end
gamma_star=max(abs(eig(A)));

%seq=[|A^0|,|A^1|,|A^2|,... 
seq2=gamma_star.^(0:empirical_k);
M_star=max(1,max(seq./seq2));





A_ineq=[0,1;-1,0;0,-1];
B_ineq=[1;-1;-gamma_star];


powers=seq(log(seq)>-600)'; % |A^k| can be so small that log(|A^k|)~=log(0)=-inf, which messes fmincon up.


options=optimoptions('fmincon','Display','off');
try
sol=fmincon(@J1,[M_star,gamma_star],A_ineq,B_ineq,[],[],[],[],@(X) NONLCON(X,powers),options);
catch ME
    fprintf(ME.message)
end
M=sol(1)+10*eps(sol(1));
gamma=sol(2)+10*eps(sol(2));

end


function [f,g]=J2(X)
    f=X(1)/((1-X(2))^2);
    g=[1/((1-X(2))^2);-2*X(1)/((X(2)-1)^3)];
end

function [f,g]=J1(X)
    f=X(1)/((1-X(2)));
    g=[1/((1-X(2))); X(1)/((X(2)-1)^2)];
end

function [f,g]=J3(X)
    f=X(1)/((1-X(2))^3);
    g=[-1/((X(2)-1)^3); 3*X(1)/((X(2)-1)^4)];
end

function [f,g]=lJ1(X)
    f=log(X(1))-1*log(1-X(2));
    g=[1/X(1); -1/(X(2)-1)];
end

function [f,g]=lJ3(X)
    f=log(X(1))-3*log(1-X(2));
    g=[1/X(1); -3/(X(2)-1)];
end


function [C,Ceq]=NONLCON(X,powers)
% Goal: for all k: |A^k|<=Mg^k
% => |A^k|/(Mg^k)<=1-eps
% if x<y then log(x) <log(y)
% => log(|A^k|)-log(Mg^k)<=log(1-eps)
% => log(|A^k|)-log(Mg^k)-log(1-eps)<=0
%     C=powers-X(1).*(X(2).^(0:numel(powers)-1))';
eps=5e-4;
    C=log(powers)-log(X(1))-log(X(2)).*(0:(numel(powers)-1))';
    C=C-log(1-eps).*(0:(numel(powers)-1))';
    Ceq=[];
end