classdef stdFigure < handle
    %stdFigure Summary of this class goes here
    %   Detailed explanation goes here

    properties
        fig;
        figId;
        XData=0;
        Name='';
        Title='';
        ax
        plots;
        plotFcn=@(x) x;
        plotLegend;
        linespec={'--',':','-.'};
        markers={'none','x','o','s','p'};
        StatusNames={'none','active','just done','done'};
        StatusLatex={'','*','$\rightarrow$','+'};
        StatusLineWidth={0.5,5,2.5,0.25}
        StatusTracker;
    end

    methods
        function obj = stdFigure(id,nplots,varargin)
            %UNTITLED4 Construct an instance of this class
            %   Detailed explanation goes here

%             obj=setProperties(obj,nargin-1,varargin{:});

            NV=cell(0,1);
            i=1;
            while i<numel(varargin)
                NV{end+1,1}=varargin{i};
                NV{end,2}=varargin{i+1};
                i=i+2;
            end


            props=fieldnames(obj);
            propsInObj=ones(size(NV,1),1);
            for i=1:size(NV,1)
                if any(strcmp(props,NV{i,1}))
                    propsInObj(i)=0;
                    obj.(NV{i,1})=NV{i,2};
                else
                    propsInObj(i)=1;
                end
            end
%             NV=NV(propsInObj==1,:);
%             set(obj,NV(:,1),NV(:,2))
%             obj= obj.attemptToSetProps(obj,NV);
            
            obj.fig=figure(id);
            clf(obj.fig);
            for i=1:size(NV,1)
                try
                    set(obj.fig,NV(i,1),NV(i,2))
                catch
                end
            end
%             obj.attemptToSetProps('fig',NV);
%             figprops=fieldnames(obj.fig);
%             for i=1:2:nargin-2
%                 if any(strcmp(figprops,varargin{i}))
%                     obj.fig.(varargin{i})=varargin{i+1};
%                 end
%             end
            obj.figId=id;
            obj.ax=axes(obj.fig);
            obj.ax.XScale='log';
            obj.ax.YScale='log';
%             set(obj.ax,NV(:,1),NV(:,2))

            title(obj.ax,'','Interpreter','latex');
            for i=1:size(NV,1)
                if strcmp(NV{i,1},'Title') && isa(NV{i,2},'String')
                    set(obj.ax.Title,'String',NV{i,2})
                else
                try
                    set(obj.ax,NV(i,1),NV(i,2))
                catch
                end
                end
            end
%             obj.attemptToSetProps('ax',NV);

%             axprops=fieldnames(obj.ax);
%             for i=1:2:nargin-2
%                 if any(strcmp(axprops,varargin{i}))
%                     obj.ax.(varargin{i})=varargin{i+1};
%                 end
%             end

            if nargin==1
                nplots=1;
            end
            hold(obj.ax,'on');
            for i=1:nplots
                for j=1:numel(obj.plotFcn)
                obj.plots{i,j}=obj.defaultPlot(i,j);
                end
            end
            obj.StatusTracker=ones(nplots,1);
            if nplots<50
            leg=legend('Interpreter','latex');
%             set(leg,NV(:,1),NV(:,2))
            for i=1:size(NV,1)
                try
                    set(leg,NV(i,1),NV(i,2))
                catch
                end
            end
            end
%             obj.attemptToSetProps(leg,NV);
%             legprops=fieldnames(leg);
%             for i=1:2:nargin-2
%                 if any(strcmp(legprops,varargin{i}))
%                     leg.(varargin{i})=varargin{i+1};
%                 end
%             end

            grid(obj.ax,'on')
            hold(obj.ax,'off')
        end

        function updatePlots(obj,objs,varargin)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here

            %Create new line-plots if there's more objs than plots
            if numel(objs)>size(obj.plots,1)
                %create new plots
                hold(obj.ax,'on');
                for i=(size(obj.plots,1)+1):numel(objs)
                    for j=1:numel(obj.plotFcn)
                        obj.plots{i,j}=obj.defaultPlot(i,j);
                    end
                end
                obj.StatusTracker=[obj.StatusTracker;ones(size(obj.StatusTracker,1)-size(obj.plots,1),1)];
                hold(obj.ax,'off');
            end
            if numel(obj.plots)>=50
                for j=1:numel(obj.fig.Children)
                    if isa(obj.fig.Children(j),'matlab.graphics.illustration.Legend')
                        delete(obj.fig.Children(j))
                        break;
                    end
                end
            end


            %update plots
            for i=1:numel(objs)
                for j=1:numel(obj.plotFcn)
                    if nargin(obj.plotFcn{j})==1
                        ydat=obj.plotFcn{j}(objs{i});
                    else
                        ydat=obj.plotFcn{j}(objs{i},varargin{:});
                    end
                    
                    if all(size(ydat)==size(obj.plots{i,j}.YData))
                        obj.plots{i,j}.YData=obj.plotFcn{j}(objs{i});
                    elseif numel(ydat)==numel(obj.plots{i,j}.YData)
                        ydat=reshape(ydat,size(obj.plots{i,j}.YData));
                        obj.plots{i,j}.YData=ydat;
                    end 

                    if nargin(obj.plotLegend{j})==2
                        obj.plots{i,j}.DisplayName=obj.plotLegend{j}(objs{i},i);
                    else
                        obj.plots{i,j}.DisplayName=obj.plotLegend{j}(objs{i},i,varargin{:});
                    end
                    
                end
            end

            if isa(obj.Title,'function_handle')
                obj.ax.Title.String=obj.Title(varargin{:});
            end
        end

        function pl= defaultPlot(obj,i,j)
            pl=plot(obj.ax,obj.XData,nan(size(obj.XData)));
            if j>1
                pl.Color=obj.plots{i,1}.Color;
                pl.LineStyle=obj.linespec{mod(j-2,numel(obj.linespec))+1};
                pl.Marker=obj.markers{floor((j-2)/numel(obj.linespec))+1};
            end
        end

        function setStatus(obj,idx,status)
%             StatusTracker
            sId=find(strcmp(obj.StatusNames,status));
            for i=idx
                for j=1:size(obj.plots,2)
                    if startsWith(obj.plots{i,j}.DisplayName,obj.StatusLatex{obj.StatusTracker(i)})
                        obj.plots{i,j}.DisplayName=erase(obj.plots{i,j}.DisplayName,obj.StatusLatex{obj.StatusTracker(i)});
                    end

                    obj.plots{i,j}.DisplayName=strjoin({obj.StatusLatex{sId},obj.plots{i,j}.DisplayName});
                    obj.plots{i,j}.LineWidth=obj.StatusLineWidth{sId};
                end
            end
            obj.StatusTracker(idx)=sId;

%             for i=idx
%                 for j=1:size(obj.plots,2)
%                     obj.plots{i,j}.DisplayName=strjoin({obj.StatusLatex{sId},obj.plots{i,j}.DisplayName});
%                 end
%             end

        end

%         function trgt= attemptToSetProps(obj,trgtName,NV)
% %             props=fieldnames(trgt);
% %             trgt=obj.(trgtName);
%             for i=1:numel(NV)
%                 if isprop(obj.(trgtName),NV{i}.Name)%any(strcmp(props,NV{i}.Name))
%                     if strcmp(class(obj.(trgtName).(NV{i}.Name)),class(NV{i}.Value))
% %                         trgt.(NV{i}.Name)=NV{i}.Value;
%                         set(obj.(trgtName),NV{i}.Name,NV{i}.Value)
%                     elseif isa(obj.(trgtName).(NV{i}.Name),'matlab.graphics.primitive.Text') && isa(NV{i}.Value,'char')
% %                         trgt.(NV{i}.Name).String=NV{i}.Value;
%                         set(obj.(trgtName).(NV{i}.Name),'String',NV{i}.Value);
% %                         set(trgt,NV{i}.Name,NV{i}.Value)
%                     end
%                 end
%             end
%         end
    end
end