classdef PAC < handle
    %UNTITLED5 Summary of this class goes here
    %   Detailed explanation goes here

    properties
        y_test;
        w_test;
        y;
        w;
        fg;
        Ns;
        N_old;
        V;
        V_L;
        Z_hat_inv;
        f0;
        f0_burnin;
        E_rho_L_hat;
        E_rho_L;
        absL_L_hat;
        absL_L_hat_delta;
        sigma_nabla;
        log_prior_fcn;
        proprnd_fcn;
        propStep;
        MH_thin;
        L_hat_priorsNs;
        L_priors;
        f_opt;
        Posterior;
        fs_prior;
        BoundConsts
        Bounds;
        NLBounds;
        Constants;
        V_L_L_hat;
        Ll=1;
        NLBoundConsts=struct('Epsi1',[],'Epsi2',[]);
        E_Betas_log_Betas=0
    end

%     properties(Access=private)
%         Quants={{@(f,fg) normL1(find_fe(f,fg));@(f,fg) normL1_alternative(find_fe(f,fg));@(f) f.barGf1;@(f) f.barGf2},...
%                 {@(f,fg) normL1(find_fe(f,fg));@(f) f.g}};
%     end

    methods
        function obj = PAC(Ns,y,w,delta,fg,f0,log_prior_fcn,proprnd_fcn,propStep,MH_thin,f_opt)
            %PAC Construct an instance of this class
            %   Detailed explanation goes here
            obj.Ns=Ns;
            obj.V=zeros(numel(Ns),1);
            obj.V_L=zeros(numel(Ns),1);
            obj.V_L_L_hat=zeros(numel(Ns),1);
            obj.Z_hat_inv=zeros(numel(Ns),1);
            obj.absL_L_hat=zeros(numel(Ns),1);
            obj.N_old=0;
            obj.f0_burnin=false;
            obj.f0=f0;
            obj.y=y;
            obj.w=w;
            ob.y_test=[];
            ob.w_test=[];
            obj.fg=fg;
            obj.log_prior_fcn=log_prior_fcn;
            obj.proprnd_fcn=proprnd_fcn;
            obj.propStep=propStep;
            obj.MH_thin=MH_thin;
            obj.L_hat_priorsNs=zeros(numel(Ns),0);
            obj.L_priors=[];
            obj.E_rho_L_hat=zeros(numel(Ns),1);
            obj.E_rho_L=zeros(numel(Ns),1);
            obj.f_opt=f_opt;
            obj.Posterior.Type='Gibbs';
            obj.Posterior.lambda=ones(numel(Ns),1);
            obj.fs_prior=cell(0,0);
            obj.BoundConsts=struct;
            obj.BoundConsts.delta=delta;
            obj.BoundConsts.EGf2=0;
            obj.BoundConsts.EGe4=0;
            obj.BoundConsts.eRenyi_arg=zeros(numel(Ns),1);
            obj.fg.mQ=max(abs(eig(obj.fg.Q)));
            obj.Bounds=zeros(numel(Ns),1);

            obj.NLBoundConsts.Epsi1=0;
            obj.NLBoundConsts.Epsi2=0;
            
        end

        function set_TrainTest(obj,y_test,w_test,y_train,w_train)
              obj.y=y_train;
              obj.w=w_train;
              obj.y_test=y_test;
              obj.w_test=w_test;
        end 
        function [new_v]=improveMCestimate(obj,Nf_it,varargin)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            if nargin<=2
                if nargin==1
                    Nf_it=10*200;
                end
                [Constants_,ConstantsNs,~,~,obj.f0,fs_prior_new]=sampleConstants_from_prior(Nf_it,obj.Ns,obj.y,obj.w,obj.f0,obj.fg,obj.log_prior_fcn,obj.proprnd_fcn,obj.propStep,obj.MH_thin);
                obj.fs_prior=fs_prior_new;
                obj.Constants=Constants_;
            else
                fs_prior_new=varargin{1};
                ConstantsNs{1}= empLossNsBulk(fs_prior_new,obj.y,obj.w,obj.Ns); %L_hats
                obj.Constants=varargin{2};
            end
%             obj.L_hat_priorsNs=[obj.L_hat_priorsNs,ConstantsNs{1}];
%             obj.L_priors=[obj.L_priors,Constants{1}];
            
%             E_rho_L_hat_old=obj.V./obj.Z_hat_inv;
%             E_rho_L_old=obj.V_L./obj.Z_hat_inv;
%             if strcmp(obj.Posterior.Type,'Gibbs')
%                 new_Betas=exp((-obj.Posterior.lambda.*ConstantsNs{1}));
%             elseif strcmp(obj.Posterior.Type,'optCentered')
%                 new_Betas=obj.optCenteredBeta(fs_prior_new);
%                 new_Betas=ones(numel(obj.Ns),1).*new_Betas';
%             end
            ConstantsNs{1}=vpa(ConstantsNs{1});
            obj.Posterior.lambda=vpa(obj.Posterior.lambda);

            new_Betas=obj.getBetas(1:numel(obj.Ns),fs_prior_new,ConstantsNs{1});
            obj.Z_hat_inv=obj.Z_hat_inv*(obj.N_old/(obj.N_old+Nf_it)) +sum(new_Betas                ,2)/(obj.N_old+Nf_it);
            obj.V        =obj.V        *(obj.N_old/(obj.N_old+Nf_it)) +sum(new_Betas.*ConstantsNs{1},2)/(obj.N_old+Nf_it);
            obj.V_L      =obj.V_L      *(obj.N_old/(obj.N_old+Nf_it)) +sum(new_Betas.*obj.Constants{1}  ,2)/(obj.N_old+Nf_it);
            obj.V_L_L_hat=obj.V_L_L_hat*(obj.N_old/(obj.N_old+Nf_it)) +sum(new_Betas.*abs(obj.Constants{1}-ConstantsNs{1})  ,2)/(obj.N_old+Nf_it);
            

            % Unbounded Renyi PAC-Bayesian bound
%             fin=cell2mat(cellfun(@(f) 1-f.g.^(obj.Ns'),fs_prior_new,'UniformOutput',false)); % 1-gamma^Ns, rows=Ns, cols=samples
%             Gf2_new=(fin.*obj.Constants{3}.*obj.Constants{4}).^2;%*(1-g^N);
            disp('Constant3')
            disp(obj.Constants{3})
            Gf2_new=(obj.Constants{3}.*obj.Constants{4}).^2;
            obj.BoundConsts.EGf2=obj.BoundConsts.EGf2.*(obj.N_old/(obj.N_old+Nf_it))+sum(Gf2_new ,2)./(obj.N_old+Nf_it);

            disp('Gf_new')
            Gf2_new

            Ge4_new=obj.Constants{2}.^4;
            obj.BoundConsts.EGe4=obj.BoundConsts.EGe4*(obj.N_old/(obj.N_old+Nf_it))+sum(Ge4_new ,2)/(obj.N_old+Nf_it);

            obj.BoundConsts.Z_hat_inv_=obj.Z_hat_inv; %col vector, one for each N in Ns


            obj.BoundConsts.eRenyi_arg=obj.BoundConsts.eRenyi_arg*(obj.N_old/(obj.N_old+Nf_it))+sum(new_Betas.^2 ,2)/(obj.N_old+Nf_it);

            obj.Bounds=obj.computeBounds();

            
            % General stuff. 
            obj.E_rho_L_hat=obj.V./obj.Z_hat_inv;
            obj.E_rho_L=obj.V_L./obj.Z_hat_inv;
%             new_v=abs(obj.E_rho_L-obj.E_rho_L_hat);
            new_v=obj.V_L_L_hat./obj.Z_hat_inv;
            obj.absL_L_hat_delta=abs(obj.absL_L_hat-new_v);
            obj.sigma_nabla=(max(abs(new_v-obj.absL_L_hat)./obj.absL_L_hat));
            obj.absL_L_hat=new_v;
            obj.N_old=obj.N_old+Nf_it;
        end

        function Bounds=computeBounds(obj)
            % computes Renyi PAC-Bayesian bound for all N\in Ns at the same
            % time.
            % returns a vector for r_N^R, 

%             obj.BoundConsts.EGf2=mean(Gf_priors.^2); %Scalar.
%             obj.BoundConsts.EGe4=mean(Ge_priors.^4); %Scalar.
%             obj.BoundConsts.Z_hat_inv_=mean(betas,2); %col vector, one for each N in Ns
%             obj.BoundConsts.eRenyi_arg=mean(betas.^2,2);%col vector
            sqrt_eRenyi=sqrt(obj.BoundConsts.eRenyi_arg)./obj.BoundConsts.Z_hat_inv_;
            
            sqrtNs=sqrt(obj.Ns);
            c1=obj.fg.mQ.*sqrt(factorial(obj.fg.p+obj.fg.q+1))./(sqrt(obj.BoundConsts.delta).*sqrtNs);
            c2=sqrt_eRenyi;
            c3=( 4*obj.fg.l1_sqr*sqrt(obj.BoundConsts.EGf2)./sqrtNs') +   6*obj.fg.p*sqrt(obj.BoundConsts.EGe4) ;
            Bounds=c1'.*c2.*c3;


        end

        function setPosterior(obj,type,magnitude)
            % types: 'Gibbs',lambda
            %       'optCentered',Sigma_inv_Scale

            if numel(magnitude)==1
                magnitude=magnitude.*ones(numel(obj.Ns),1);
            end

            if strcmp(type,'Gibbs')
                obj.Posterior.Type='Gibbs';
                obj.Posterior.lambda=magnitude;
            elseif strcmp(type,'optCentered')
                obj.Posterior.Type='optCentered';
                obj.Posterior.SigmaMag=magnitude;
            end
        end

        function Betas = getBetas(obj,i,fs_prior,L_hat_priorsNs_)
            % returns betas\propto \rho(f)/\pi(f)
            % i is an index to obj.Ns array
            % if Posterior.Type=='optCentered':
            %       x=obj.fs_prior is a cell array of 1 or 2 dimensions
            % if Posterior.Type=='Gibbs'
            %       x=obj.L_hat_priorsNs is a matrix of 1 or 2 dimensions
            %       containing empirical loss samples from prior.
            
            if strcmp(obj.Posterior.Type,'Gibbs')
                e=vpa(-obj.Posterior.lambda(i)'.*L_hat_priorsNs_(i,:));
                Betas=exp(e);
            elseif strcmp(obj.Posterior.Type,'optCentered')
                Betas=obj.optCenteredBeta(fs_prior,i);
            end
        end

        function betas = optCenteredBeta(obj,fs,idx)
            % beta \propto \frac{\rho}{\pi}
            % \hat{\rho}=exp(-(\theta-\theta_0)^T\Sigma^{-1}(\theta-\theta_0))
            % \hat{\pi}=exp(log_prior_fcn(f))

            theta_0=[reshape(obj.f_opt.A,[],1);reshape(obj.f_opt.B,[],1);reshape(obj.f_opt.C,[],1);reshape(obj.f_opt.D,[],1)];
            
            betas=zeros(numel(idx),numel(fs));
            for i=1:numel(idx)
                mag=obj.Posterior.SigmaMag(i);
                Sigma_inv=mag*diag(ones(numel(theta_0),1));
                l_prior=obj.log_prior_fcn;
                if i==1 || obj.Posterior.SigmaMag(i)~=obj.Posterior.SigmaMag(i-1)
                    parfor j=1:size(fs,2)
                        theta=[reshape(fs{j}.A,[],1);reshape(fs{j}.B,[],1);reshape(fs{j}.C,[],1);reshape(fs{j}.D,[],1)];
                        d=theta-theta_0;
                        betas(i,j)=exp(-d'*Sigma_inv*d-l_prior(fs{j}));
                    end
                else
                    betas(i,:)=betas(i-1,:);
                end
            end

        end

        function generateDataMod(obj,Nmax,noiseType,varargin)
        %%generateData Generates input/output data
        % Options: 'Gaussian','Trunc_Gaussian'
            fprintf('Generating [y,w] \n Please wait... \n')
            p=obj.fg.p;
            q=obj.fg.q;
            v_init=nan(p+q,1e3);
            v=nan(p+q,Nmax);
            if nargin>3
                Q=varargin{1};
            else
                Q=obj.fg.Q;
            end
            R=chol(Q)';
            
            if strcmp(noiseType,'Gaussian')
                    v_init=R*randn(p+q,1e3);
                    v=R*randn(p+q,Nmax);
            elseif strcmp(noiseType,'Trunc_Gaussian')
                    obj.fg.ce=varargin{2};
                    ce_=obj.fg.ce/max(sum(R,2));
                    pd = makedist('Normal');
                    t = truncate(pd,-ce_,ce_);
                    r = random(t,p+q,1e3);
                    v_init=R*r;
                    r = random(t,p+q,Nmax);
                    v=R*r;
            end
            
            n=obj.fg.n;

            x=zeros(n,1);
            for i=1:size(v_init,2)
               x=obj.fg.A*x+obj.fg.B*v_init(:,i);
            end
            y_=zeros(p,Nmax);
            w_=zeros(q,Nmax);

            A=obj.fg.A;
            B=obj.fg.B;
            Cy=obj.fg.C(1:p,:);
            Cw=obj.fg.C(p+1:end,:);
            Dy=obj.fg.D(1:p,:);
            Dw=obj.fg.D(p+1:end,:);

            for i=1:Nmax
               y_(:,i)=Cy*x+Dy*v(:,i);
               w_(:,i)=Cw*x+Dw*v(:,i);
               x=A*x+B*v(:,i);
            end
            obj.y=y_;
            obj.w=[y_; w_];
            fprintf('Done generating [y,w]\n')

        end




    %end

        function generateData(obj,Nmax,noiseType,varargin)
        %%generateData Generates input/output data
        % Options: 'Gaussian','Trunc_Gaussian'
            fprintf('Generating [y,w] \n Please wait... \n')
            p=obj.fg.p;
            q=obj.fg.q;
            v_init=nan(p+q,1e3);
            v=nan(p+q,Nmax);
            if nargin>3
                Q=varargin{1};
            else
                Q=obj.fg.Q;
            end
            R=chol(Q)';
            
            if strcmp(noiseType,'Gaussian')
                    v_init=R*randn(p+q,1e3);
                    v=R*randn(p+q,Nmax);
            elseif strcmp(noiseType,'Trunc_Gaussian')
                    obj.fg.ce=varargin{2};
                    ce_=obj.fg.ce/max(sum(R,2));
                    pd = makedist('Normal');
                    t = truncate(pd,-ce_,ce_);
                    r = random(t,p+q,1e3);
                    v_init=R*r;
                    r = random(t,p+q,Nmax);
                    v=R*r;
            end
            
            n=obj.fg.n;

            x=zeros(n,1);
            for i=1:size(v_init,2)
               x=obj.fg.A*x+obj.fg.B*v_init(:,i);
            end
            y_=zeros(p,Nmax);
            w_=zeros(q,Nmax);

            A=obj.fg.A;
            B=obj.fg.B;
            Cy=obj.fg.C(1:p,:);
            Cw=obj.fg.C(p+1:end,:);
            Dy=obj.fg.D(1:p,:);
            Dw=obj.fg.D(p+1:end,:);

            for i=1:Nmax
               y_(:,i)=Cy*x+Dy*v(:,i);
               w_(:,i)=Cw*x+Dw*v(:,i);
               x=A*x+B*v(:,i);
            end
            obj.y=y_;
            obj.w=w_; %[y_; w_];
            fprintf('Done generating [y,w]\n')

        end




    end


    methods(Static=true)

        
        function [f_end,Q,acc,rej,Fs] = customMH_parfor(f0,n,lpos,proprnd_fcn,thin,N_threads,quant,acc,rej)
        %customMH_parfor Metropolis-Hastings algorithm for sampling
        %predictors
        % f0 is initial point
        % n  is number of samples to generate
        % lpos is the log-likelihood function 
        % proprnd_fcn is the propogation function (must be symmetric)
        % thin - thinning parameter, how many samples to skip
        % N_threads - how many threads to use
        % quant - a collumn cell array of functions to evaluate the MCMC samples on
        %         i.e. quant= {@(x) x^2; @(x)sqrt(x); @(x)some_function(x)};
        % acc - how many samples have been accepted before (to continue this
        %       statistic)
        % rej - how many samples have been rejected before
        %
        % f_end - the last monte-carlo sample, to be used as f0 in next fcn call
        % Q - is (numel(quant) by n) matrix containing the function valuations
        % acc,rej - acceptance rate statistic
        
        
        fs=cell(N_threads,1);
        old_lpos=cell(N_threads,1);
        n_quant=numel(quant);
        Quants=cell(N_threads);
        % acc=zeros(N_threads,1);
        % rej=zeros(N_threads,1);
        
        
        % lu=log(rand(1,n));
        
        %% check initial condition
        old_lpos0=lpos(f0);
        
        attempts=0;
        fp=f0;
        while isinf(old_lpos0)&&attempts<10000 % invalid initial point, seeing if there's valid points in the neighborhood
            fp=proprnd_fcn(f0);
            [old_lpos0,~,fp]=lpos(fp);
            attempts=attempts+1;
        end
        
        if isinf(lpos(fp))
            error('Invalid initial point')
        elseif (isinf(lpos(f0)))
            warning('Invalid initial point, but found a valid point in the neighborhood')
            f0=fp;
        end
        
        %% Main loop
        
        f_s_idx=cell(1,N_threads);
        old_lpos=cell(1,N_threads);
        f_s=cell(1,N_threads);
        for i=1:N_threads
            fs{i}=f0;
            old_lpos{i}=old_lpos0;
            Quants_{i}=nan(n_quant,floor(n/N_threads));
            f_s{i}=cell(1,floor(n/N_threads));
            f_s_idx{i}=0;
        end
        
        acc=ones(1,N_threads)*acc;
        rej=ones(1,N_threads)*rej;
        
        parfor th=1:N_threads
            lu_th=log(rand(1,floor(n/N_threads)*thin+thin));
        
            do=floor(n/N_threads);
            if th==N_threads
                do=n-floor(n/N_threads)*(N_threads-1)
            end
            lu_th=log(rand(1,do*thin+thin));
        
            for i=1:do
                f=fs{th};
                for thin_i=1:thin
                    fp=proprnd_fcn(f);
                    [lp,~,fp]=lpos(fp);
                
                    lH=lp-old_lpos{th};
                    if lu_th(thin*i+thin_i)<lH 
                        %keep
                        f=fp;
                        old_lpos{th}=lp;
                        acc(th)=acc(th)+1;
                    else
                        rej(th)=rej(th)+1;
                        %don't keep
                    end
                end
                fs{th}=f;
                Quants_{th}(:,i)=cellfun(@(fun) fun(f),quant);%quant(f);
                f_s_idx{th}=f_s_idx{th}+1;
                f_s{th}{f_s_idx{th}}=f;
                
            end
        %     fprintf('th:%i, acc_rate=%f\n',th,acc(th)/(acc(th)+rej(th)));
        end
        
        nth=floor(n/N_threads);
        Q=nan(n_quant,n);
        Fs=cell(1,n);
        for i=1:N_threads
        %     nth=numel(f_s{i});
            idx=(i-1)*nth+1;
            Fs(idx:(idx+numel(f_s{i})-1))=f_s{i};
            Q(:,idx:(idx+numel(f_s{i})-1))=Quants_{i};
        end
        f_end=fs{randi(N_threads)};
        
        acc=sum(acc);
        rej=sum(rej);
        
        end
    end
end