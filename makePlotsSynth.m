filename='pac_sqrtn_real.mat'


oldpath=path;
path(oldpath,'functions/M2tikz')




load(filename) %
pac_n=numel(pac);

fig=figure(3);
fig.Name='E_L and E_L_hat';
% ConstantsNsAxes=axes(fig,'XScale','log');
pl_E_rho_L_hats=cell(pac_n,1);
pl_E_rho_L=cell(pac_n,1);
for i=1:pac_n
pl_E_rho_L_hats{i}=loglog(pac{i}.Ns,pac{i}.E_rho_L_hat);
pl_E_rho_L_hats{i}.DisplayName=sprintf('$\\omega_%i:\\;E_{f\\sim\\rho}\\hat{\\mathcal{L}}_N(f)$',i);

hold on
end
for i=1:pac_n
pl_E_rho_L{i}=loglog(pac{i}.Ns,pac{i}.E_rho_L,'--');
pl_E_rho_L{i}.DisplayName=sprintf('$\\omega_%i:\\;E_{f\\sim\\rho}\\mathcal{L}_N(f)$',i);

hold on
end
% pl_E_rho_L_hats.Parent.Title.Interpreter="latex";
legend('Interpreter','latex');
grid on
hold off


fig=figure(1);
fig.Name='E_rho |L-L_hat|';
pl_E_rho_diff=cell(pac_n,1);
for i=1:pac_n
pl_E_rho_diff{i}=loglog(pac{i}.Ns,pac{i}.absL_L_hat);
pl_E_rho_diff{i}.DisplayName=sprintf('$\\omega_%i:\\;E_{f\\sim\\rho}|\\mathcal{L}_N(f)-\\hat{\\mathcal{L}}_N(f)|$',i);

hold on
end
% pl_E_rho_L_hats.Parent.Title.Interpreter="latex";
legend('Interpreter','latex');
grid on
hold off


fig=figure(5);
fig.Name='Bounds';
pl_Bounds=cell(pac_n,1);
for i=1:pac_n
pl_Bounds{i}=loglog(pac{i}.Ns,pac{i}.Bounds);
pl_Bounds{i}.DisplayName=sprintf('$\\omega_%i:\\;r_N^R$',i);

hold on
end
legend('Interpreter','latex');
grid on
hold off

fig=figure(6);
fig.Name='E_rho |L-L_hat| and Bounds';
for i=1:pac_n
a=loglog(pac{i}.Ns,pac{i}.absL_L_hat,'-r');
% a.DisplayName=sprintf('$\\omega_%i:\\;E_{f\\sim\\rho}|\\mathcal{L}_N(f)-\\hat{\\mathcal{L}}_N(f)|$',i);
a.DisplayName=sprintf('$E_{f\\sim\\rho}|\\mathcal{L}(f)-\\hat{\\mathcal{L}}_N(f)|$');

hold on
b=loglog(pac{i}.Ns,pac{i}.Bounds,'-b');
% b.Color=a.Color;
% b.DisplayName=sprintf('$\\omega_%i:\\;r_N^R$',i);
b.DisplayName=sprintf('$r_N^R$');
end
% pl_E_rho_L_hats.Parent.Title.Interpreter="latex";
legend([a,b],'Interpreter','latex','Location','northeast');
grid on
hold off
% title('$\rho(f)\propto exp(-\lambda_N\hat{\mathcal{L}}_N(f))$, $\lambda_N=\sqrt{N}$','Interpreter','latex')
fig.Units="inches";
linewidth=3;
aspect=4/3;
fig.Position(3:4)=[linewidth,linewidth/aspect];
xticks([1e1,1e2,1e3,1e4,1e5])
% savefig('Renyi.fig')
% exportgraphics(fig,'Renyi.eps')
exportgraphics(fig,'Renyi.png')
% matlab2tikz('Renyi.tex')

%%
load(filename   ) %
pac_n=numel(pac);
fig=figure(7);
fig.Name='E_rho |L-L_hat| and Bounds and exp(Renyi)';
left_color=[0,0,0];
right_color=[0,1,0];
set(fig,'defaultAxesColorOrder',[left_color; right_color]);

for i=1:pac_n

%hold on
a=loglog(pac{i}.Ns,pac{i}.absL_L_hat,'-r');
% a.DisplayName=sprintf('$\\omega_%i:\\;E_{f\\sim\\rho}|\\mathcal{L}_N(f)-\\hat{\\mathcal{L}}_N(f)|$',i);
a.DisplayName=sprintf('$E_{f\\sim\\rho}|\\mathcal{L}(f)-\\hat{\\mathcal{L}}_N(f)|$');

hold on
b=loglog(pac{i}.Ns,pac{i}.Bounds,'-b');
% b.Color=a.Color;
% b.DisplayName=sprintf('$\\omega_%i:\\;r_N^R$',i);
b.DisplayName=sprintf('$r_N$');
%hold off
yyaxis right
set(gca,'YColor',right_color)
% hold on
c=semilogx(pac{i}.Ns,sqrt(pac{i}.BoundConsts.eRenyi_arg)./pac{i}.BoundConsts.Z_hat_inv_,'-g');
c.DisplayName=sprintf('$\\bar{D}_2(\\rho|\\pi)$');
hold on
% hold off
yyaxis left



end
% pl_E_rho_L_hats.Parent.Title.Interpreter="latex";
legend([a,b,c],'Interpreter','latex','Location','northeast');
grid on
hold off
% title('$\rho(f)\propto exp(-\lambda_N\hat{\mathcal{L}}_N(f))$, $\lambda_N=\sqrt{N}$','Interpreter','latex')
fig.Units="inches";
linewidth=3;
aspect=4/3;
fig.Position(3:4)=[linewidth,linewidth/aspect];
xticks(logspace(1,5,4))
% savefig('RenyiwD2.fig')
% exportgraphics(fig,'RenyiwD2.eps')
exportgraphics(fig,'RenyiwD2.png')
% matlab2tikz('RenyiwD2.tex')
